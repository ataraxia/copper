const { EmbedBuilder, ActionRowBuilder, ModalBuilder, TextInputBuilder, TextInputStyle } = require("discord.js");
const Keyv = require("keyv");
// greetz to https://bobbyhadz.com/blog/javascript-error-err-require-esm-of-es-module-node-fetch
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const md5 = require("md5");

exports.SlashCompanions = [
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            var subsystem = interaction.options.getNumber("subsystem");
            if(subsystem === null)
                subsystem = 0;
            var image = interaction.options.getAttachment("image");
            if(image!==null) {
                if(!(image.contentType.includes("image/png")||image.contentType.includes("image/jpeg")||image.contentType.includes("image/webp")||image.contentType.includes("image/gif"))) {
                    image = null;
                }
            }
            const username = interaction.options.getString("user");
            const message = interaction.options.getString("message");
            (async () => {
                try {
                    let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        await interaction.reply({ content: "The user you tried to proxy as has not been added yet.\nCode: NADDED", ephemeral: true });
                    } else {
                        kuser = JSON.parse(kuser);
                        for(const userkey in kuser.subs.at(subsystem).users) {
                            const user = kuser.subs.at(subsystem).users.at(userkey);
                            if(user.name === username) {
                                const embedMessage = new EmbedBuilder()
                                    .setColor(parseInt(user.color,16))
                                    .setThumbnail(user.thumb)
                                    .setTitle(`${user.name} ${kuser.subs.at(subsystem).tag===null?"":kuser.subs.at(subsystem).tag} :`)
                                    .addFields({ name: "Message:", value: message.slice(0,1024), inline: true })
                                    .setTimestamp();
                                if(image!==null)
                                    embedMessage.setImage(image.url);
                                kuser.lastFronted = `${subsystem} ${username}`;
                                await kcl.set(`${md5(interaction.user.tag)}`,JSON.stringify(kuser));
                                await interaction.reply({ embeds: [embedMessage] });
                                return;
                            }
                        }
                        await interaction.reply({ content: "The user specified has not been added to this system yet.\nCode: NADDED", ephemeral: true });
                    }
                } catch(error) {}
            })();
        },
        "name": "proxy"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            var image = interaction.options.getAttachment("image");
            if(image!==null) {
                if(!(image.contentType.includes("image/png")||image.contentType.includes("image/jpeg")||image.contentType.includes("image/webp")||image.contentType.includes("image/gif"))) {
                    image = null;
                }
            }
            const message = interaction.options.getString("message");
            (async () => {
                try {
                    let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        await interaction.reply({ content: "The user you tried to proxy as has not been added yet.\nCode: NADDED", ephemeral: true });
                    } else {
                        kuser = JSON.parse(kuser);
                        const lastFrontedSub = parseInt(kuser.lastFronted.split(" ")[0]);
                        if(lastFrontedSub===undefined) {
                            return;
                        }
                        const lastFrontedUser = kuser.lastFronted.split(" ")[1];
                        for(const userkey in kuser.subs.at(lastFrontedSub).users) {
                            const user = kuser.subs.at(lastFrontedSub).users.at(userkey);
                            if(user.name === lastFrontedUser) {
                                var mes = message.slice(0,1024);
                                mes = mes.replaceAll(":NEWLINE:","\n");
                                mes = mes.replaceAll("\\n","\n");
                                const embedMessage = new EmbedBuilder()
                                    .setColor(parseInt(user.color,16))
                                    .setThumbnail(user.thumb)
                                    .setTitle(`${user.name} ${kuser.subs.at(lastFrontedSub).tag===null?"":kuser.subs.at(lastFrontedSub).tag} :`)
                                    .addFields({ name: "Message:", value: mes, inline: true })
                                    .setTimestamp();
                                if(image!==null)
                                    embedMessage.setImage(image.url);
                                await interaction.reply({ embeds: [embedMessage] });
                                return;
                            }
                        }
                        await interaction.reply({ content: "The user specified has not been added to this system yet.\nCode: NADDED", ephemeral: true });
                    }
                } catch(error) {}
            })();
        },
        "name": "latch"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            (async () => {
                try {
                    let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        return;
                    } else {
                        kuser = JSON.parse(kuser);
                        kuser.reply = `${interaction.targetMessage.author.tag}:\n${interaction.targetMessage.content}`;
                        kuser.replyUrl = interaction.targetMessage.url;
                        await kcl.set(`${md5(interaction.user.tag)}`,JSON.stringify(kuser));
                    }
                    const modal = new ModalBuilder()
                        .setCustomId("replyLatchModal")
                        .setTitle("Reply...");
                    const replyInput = new TextInputBuilder()
                        .setCustomId('replyInput')
                        .setLabel("What would you like to send?")
                        .setStyle(TextInputStyle.Paragraph)
                        .setRequired(true);
                    const firstActionRow = new ActionRowBuilder().addComponents(replyInput);
                    modal.addComponents(firstActionRow);
                    const filter = (interaction) => interaction.customId == 'replyLatchModal';
                    await interaction.showModal(modal)
                    await interaction.awaitModalSubmit({ filter, time: 15_000 })
                        .then(interaction => {
                            const message = interaction.fields.getTextInputValue("replyInput");
                            (async () => {
                                let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                                if(kuser === undefined) {
                                    return;
                                } else {
                                    kuser = JSON.parse(kuser);
                                    const lastFrontedSub = parseInt(kuser.lastFronted.split(" ")[0]);
                                    if(lastFrontedSub===undefined) {
                                        return;
                                    }
                                    const lastFrontedUser = kuser.lastFronted.split(" ")[1];
                                    for(const userkey in kuser.subs.at(lastFrontedSub).users) {
                                        const user = kuser.subs.at(lastFrontedSub).users.at(userkey);
                                        if(user.name === lastFrontedUser) {
                                            var mes = message.slice(0,1024);
                                            var rep = kuser.reply.slice(0,1024);
                                            if(rep.split("\n")[1] == null || rep.split("\n")[1].trim() === '') {
                                                rep = kuser.replyUrl;
                                            }
                                            const embedMessage = new EmbedBuilder()
                                                .setColor(parseInt(user.color,16))
                                                .setThumbnail(user.thumb)
                                                .setTitle(`${user.name} ${kuser.subs.at(lastFrontedSub).tag===null?"":kuser.subs.at(lastFrontedSub).tag} :`)
                                                .addFields([{ name: "In reply to:", value: rep, inline: false },{ name: "Message:", value: mes, inline: true }])
                                                .setTimestamp();
                                            await interaction.reply({embeds: [embedMessage]});
                                            return;
                                        }
                                    }
                                }
                            })();
                        })
                        .catch();
                } catch(error) {}
            })();
        },
        "name": "Reply (Latch)"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            (async () => {
                try {
                    let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        return;
                    } else {
                        kuser = JSON.parse(kuser);
                        kuser.reply = `${interaction.targetMessage.author.tag}:\n${interaction.targetMessage.content}`;
                        kuser.replyUrl = interaction.targetMessage.url;
                        await kcl.set(`${md5(interaction.user.tag)}`,JSON.stringify(kuser));
                    }
                    const modal = new ModalBuilder()
                        .setCustomId("replyUserModal")
                        .setTitle("Reply...");
                    const replyInput = new TextInputBuilder()
                        .setCustomId('userReplyInput')
                        .setLabel("What would you like to send?")
                        .setStyle(TextInputStyle.Paragraph)
                        .setRequired(true);
                    const userInput = new TextInputBuilder()
                        .setCustomId('userInput')
                        .setLabel("What is the value of the desired user?")
                        .setStyle(TextInputStyle.Short)
                        .setRequired(true);
                    const subsInput = new TextInputBuilder()
                        .setCustomId('subsInput')
                        .setLabel("What is the desired subsystem?")
                        .setStyle(TextInputStyle.Short)
                        .setRequired(false);
                    const firstActionRow = new ActionRowBuilder().addComponents(replyInput);
                    const secondActionRow = new ActionRowBuilder().addComponents(userInput);
                    const thirdActionRow = new ActionRowBuilder().addComponents(subsInput);
                    modal.addComponents(firstActionRow,secondActionRow,thirdActionRow);
                    const filter = (interaction) => interaction.customId == 'replyUserModal';
                    await interaction.showModal(modal)
                    await interaction.awaitModalSubmit({ filter, time: 15_000 })
                        .then(interaction => {
                            const message = interaction.fields.getTextInputValue("userReplyInput");
                            const username = interaction.fields.getTextInputValue("userInput");
                            var subsystem = interaction.fields.getTextInputValue("subsInput");
                            if(subsystem === null) {
                                subsystem = "0";
                            }
                            subsystem = parseInt(subsystem);
                            (async () => {
                                let kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                                if(kuser === undefined) {
                                    return;
                                } else {
                                    kuser = JSON.parse(kuser);
                                    for(const userkey in kuser.subs.at(subsystem).users) {
                                        const user = kuser.subs.at(subsystem).users.at(userkey);
                                        if(user.name === username) {
                                            var mes = message.slice(0,1024);
                                            var rep = kuser.reply.slice(0,1024);
                                            if(rep.split("\n")[1] == null || rep.split("\n")[1].trim() === '') {
                                                rep = kuser.replyUrl;
                                            }
                                            const embedMessage = new EmbedBuilder()
                                                .setColor(parseInt(user.color,16))
                                                .setThumbnail(user.thumb)
                                                .setTitle(`${user.name} ${kuser.subs.at(subsystem).tag===null?"":kuser.subs.at(subsystem).tag} :`)
                                                .addFields([{ name: "In reply to:", value: rep, inline: false },{ name: "Message:", value: mes, inline: true }])
                                                .setTimestamp();
                                            await interaction.reply({embeds: [embedMessage]});
                                            return;
                                        }
                                    }
                                }
                            })();
                        })
                        .catch(console.error);
                } catch(error) {console.error(error)}
            })();
        },
        "name": "Reply (User)"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            var subsystem = interaction.options.getNumber("subsystem");
            if(subsystem === null)
                subsystem = 0;
            const username = interaction.options.getString("username");
            (async () => {
                try {
                    var kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined||JSON.parse(kuser).subs.length === 0) {
                        let system = {
                            "subs": [{
                                "users": [
                                    {
                                        "name": `${username}`,
                                        "color": "000000",
                                        "thumb": "https://cdn.discordapp.com/app-icons/1013337418172207114/9f92713d7b1103491dd9a8660a014ee5.png",
                                        "json": "",
                                        "pronouns": "",
                                        "birthday": "",
                                    }
                                ],
                                "tag": "",
                                "name": "",
                                "info": ""
                            }]
                        };
                        kuser = JSON.stringify(system);
                    } else {
                        const kparsed = JSON.parse(kuser);
                        kparsed.subs.at(subsystem).users += {"name": `${username}`, "color": "000000", "thumb": "https://cdn.discordapp.com/app-icons/1013337418172207114/9f92713d7b1103491dd9a8660a014ee5.png", "json": "", "pronouns": ""};
                        kuser = JSON.stringify(kparsed);
                    }
                    await kcl.set(`${md5(interaction.user.tag)}`, kuser);
                    await interaction.reply({ content: `User ${username} added.`, ephemeral: true });
                } catch(error) {}
            })();
        },
        "name": "adduser"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            var subsystem = interaction.options.getNumber('subsystem');
            if(subsystem === null)
                subsystem = 0;
            var all = interaction.options.getBoolean('all');
            if(all === null)
                all = false;
            const username = interaction.options.getString('username');
            (async () => {
                try {
                    var kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        await interaction.reply({ content: "You must have a headspace to delete a user or subsystem.\nCode: NHEADSP", ephemeral: true });
                        return;
                    } else {
                        const kparsed = JSON.parse(kuser);
                        if(all === true) {
                            kparsed.subs.splice(subsystem, 1);
                            kuser = JSON.stringify(kparsed);
                            await interaction.reply({ content: `Subsystem ${subsystem} removed.`, ephemeral: true });
                        } else {
                            var userArr = [];
                            for(const user of kparsed.subs.at(subsystem).users) {
                                if(username === user.name) {
                                    continue;
                                }
                                userArr.push(user);
                            }
                            kparsed.subs.at(subsystem).users = userArr;
                            kuser = JSON.stringify(kparsed);
                            await interaction.reply({ content: `User ${username} of subsystem ${subsystem} removed.`, ephemeral: true });
                        }
                        await kcl.set(`${md5(interaction.user.tag)}`,kuser);
                    }
                } catch(error) {}
            })();
        },
        "name": "deluser"
    },
    {
        execute: function (interaction) {
            const user = interaction.options.getString("user");
            var subsystem = interaction.options.getNumber("subsystem");
            const birthday = interaction.options.getString("birthday");
            const tag = interaction.options.getString("tag");
            const color = interaction.options.getString("color");
            const thumbnail = interaction.options.getString("thumbnail");
            const misc = interaction.options.getString("misc");
            const pronouns = interaction.options.getString("pronouns");
            const name = interaction.options.getString("name");
            const kcl = new Keyv('sqlite://systems.db');
            (async () => {
                try {
                    var kuser = await kcl.get(`${md5(interaction.user.tag)}`);
                    if(kuser === undefined) {
                        // halt and catch fire
                        return;
                    } else {
                        kuser = JSON.parse(kuser);
                    }
                    const changeInfos = (user,subsystem,birthday,tag,name,color,thumbnail,misc,pronouns,kuser,interaction) => {
                        (async () => {
                            try {
                                if(tag !== null) {
                                    kuser.subs.at(subsystem).tag = tag;
                                }
                                var reply = "```";
                                for(const i in kuser.subs.at(subsystem).users) {
                                    if(user === kuser.subs.at(subsystem).users.at(i).name) {
                                        if(birthday !== null) {
                                            kuser.subs.at(subsystem).users.at(i).birthday = birthday;
                                            reply += `User's birthday changed to ${birthday}.\n`;
                                        }
                                        if(color !== null) {
                                            kuser.subs.at(subsystem).users.at(i).color = color;
                                            reply += `User's embed color changed to ${color}.\n`;
                                        }
                                        if(name !== null) {
                                            kuser.subs.at(subsystem).users.at(i).name = name;
                                            reply += `User's name changed to ${name}.\n`;
                                        }
                                        if(thumbnail !== null) { 
                                            kuser.subs.at(subsystem).users.at(i).thumb = thumbnail;
                                            reply += `User's thumbnail image URL changed to ${thumbnail}.\n`;
                                        }
                                        if(misc !== null) {
                                            kuser.subs.at(subsystem).users.at(i).json = misc;
                                            reply += `User's misc. info changed to ${misc}.\n`;
                                        }
                                        if(pronouns !== null) {
                                            kuser.subs.at(subsystem).users.at(i).pronouns = pronouns;
                                            reply += `User's pronouns changed to ${pronouns}.\n`;
                                        }
                                    }
                                }
                                reply += "```";
                                await interaction.reply({content: reply, ephemeral: true});
                            } catch (error) {}
                        })();
                    };
                    if(subsystem !== null) {
                        changeInfos(user,subsystem,birthday,tag,name,color,thumbnail,misc,pronouns,kuser,interaction);
                        await kcl.set(`${md5(interaction.user.tag)}`,JSON.stringify(kuser));
                        return;
                    } else {
                        subsystem = 0;
                        changeInfos(user,subsystem,birthday,tag,name,color,thumbnail,misc,pronouns,kuser,interaction);
                        await kcl.set(`${md5(interaction.user.tag)}`,JSON.stringify(kuser));
                        return;
                    }
                } catch(error) {}
            })();
            return;
        },
        "name": "change"
    },
    {
        execute: function (interaction) {
            const tag = interaction.options.getString("tag");
            const headspace = interaction.options.getBoolean("headspace");
            var subsystem = interaction.options.getNumber("subsystem");
            const user = interaction.options.getString("user");
            const kcl = new Keyv('sqlite://systems.db');
            (async () => {
                try {
                    var kuser = await kcl.get(`${md5(tag)}`);
                    if(kuser === undefined) {
                        // halt and catch fire
                        return;
                    } else {
                        kuser = JSON.parse(kuser);
                    }
                    if(headspace===true) {
                        if(kuser.subs === undefined) {
                            // halt and catch fire
                            return;
                        } else {
                            var compiled = "```";
                            var index = 0;
                            for(const {tag: t, name: n, info: i} of kuser.subs) {
                                compiled += `About subsystem ${n}:\n\nTag: ${t}\nInfo: ${i}\n\n\nUser information in this subsystem:\n\n`;
                                for(const {name: n2, json: j, pronouns: p, birthday: b} of kuser.subs.at(index).users) {
                                    compiled += `About ${n2} ${t} :\nInfo: ${j}\nThis user's pronouns are ${p}\nThis user's birthday is ${b}\n`;
                                    compiled += `\n---\n\n`;
                                }
                                index += 1;
                            }
                            compiled += "End of information```";
                            await interaction.reply({content: compiled, ephemeral: true});
                            return;
                        }
                    } else {
                        const getInfo = (kuser,interaction,subsystem) => {
                            (async () => {
                                var info = "```";
                                for(const {name: n, json: j, pronouns: p, birthday: b} of kuser.subs.at(subsystem).users) {
                                    if(user === n) {
                                        info += `About ${n} ${kuser.subs.at(subsystem).tag} :\nInfo: ${j}\nThis user's pronouns are ${p}\nThis user's birthday is ${b}`;
                                    }
                                }
                                info += "```";
                                await interaction.reply({content: info, ephemeral: true});
                            })();
                        };
                        if(subsystem !== null) {
                            if(user !== null) {
                                getInfo(kuser,interaction,subsystem);
                                return;
                            } else {
                                var compiled = `\`\`\`About subsystem ${kuser.subs.at(subsystem).name}:\n\nTag: ${kuser.subs.at(subsystem).tag}\nInfo: ${kuser.subs.at(subsystem).info}\n\n\nUser information in this subsystem:\n\n`;
                                for(const {name: n, json: j, pronouns: p, birthday: b} of kuser.subs.at(subsystem).users) {
                                    compiled += `About ${n} ${kuser.subs.at(subsystem).tag} :\nInfo: ${j}\nThis user's pronouns are ${p}\nThis user's birthday is ${b}\n\n`;
                                }
                                compiled += "End of information```";
                                await interaction.reply({content: compiled, ephemeral: true});
                                return;
                            }
                        } else {
                            subsystem = 0;
                            if(user !== null) {
                                getInfo(kuser,interaction,subsystem);
                                return;
                            } else {
                                await interaction.reply({content: "You must specify one of the optional parameters.\nCode: NENOUGH", ephemeral: true});
                                return;
                            }
                        }
                    }
                } catch(error) {console.error(error);}
            })();
        },
        "name": "about"
    },
    {
        execute: function (interaction) {
            const kcl = new Keyv('sqlite://systems.db');
            var subsystem = interaction.options.getNumber("subsystem");
            if(subsystem === null)
                subsystem = 0;
            const jsonurl = interaction.options.getString("jsonurl");
            try {
                fetch(jsonurl)
                    .then(res => {
                        res.status = res.status;
                        (async () => { 
                            try {
                                var system = await kcl.get(`${md5(interaction.user.tag)}`);
                                var jparsed = await res.json();
                                if(system === undefined) {
                                    system = {
                                        'subs': [{
                                            'users': [],
                                            'tag': jparsed.tag,
                                            'name': jparsed.name,
                                            'info': jparsed.description
                                        }]
                                    };
                                    var userArr = [];
                                    for(const {name: n2, color: c, avatar_url: au, description: d2, pronouns: p, birthday: b} of jparsed.members) {
                                        userArr.push({
                                            "name": n2,
                                            "color": c===null?000000:c,
                                            "thumb": au,
                                            "json": d2,
                                            "pronouns": p,
                                            "birthday": b,
                                        });
                                    }
                                    system.subs.at(0).users = userArr;
                                } else {
                                    system = JSON.parse(system);
                                    system.subs.splice(subsystem,0,{'users': [], 'tag': "", 'name': "", 'info': ""});
                                    system.subs.at(subsystem).tag = jparsed.tag;
                                    system.subs.at(subsystem).name = jparsed.name;
                                    system.subs.at(subsystem).info = jparsed.description;
                                    var userArr = [];
                                    for(const {name: n2, color: c, avatar_url: au, description: d2, pronouns: p, birthday: b} of jparsed.members) {
                                        userArr.push({
                                            "name": n2,
                                            "color": c===null?000000:c,
                                            "thumb": au,
                                            "json": d2,
                                            "pronouns": p,
                                            "birthday": b,
                                        });
                                        system.subs.at(subsystem).users = userArr;
                                    }
                                }
                                await kcl.set(`${md5(interaction.user.tag)}`, JSON.stringify(system));
                                await interaction.reply({content: "Your PluralKit system has been imported into Copper.", ephemeral: true});
                            } catch(error) {}
                        })();
                        return;
                    })
            } catch(error) {}
        },
        "name": "importsystem"
    },{
        execute: function (interaction) {
            (async () => {
                await interaction.reply({content: "https://discord.com/api/oauth2/authorize?client_id=1013337418172207114&permissions=277025393664&scope=bot%20applications.commands", ephemeral: true});
            })();
        },
        "name": "invite"
    }
];
