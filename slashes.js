exports.Slashes = {
    "main": [{
        "name": "proxy",
        "description": "Proxies a message through the bot as you.",
        "options": [
            {
                "name": "user",
                "description": "Who you are.",
                "type": 3,
                "required": true, 
            },
            {
                "name": "message",
                "description": "What you want to say.",
                "type": 3,
                "required": true,
            },
            {
                "name": "subsystem",
                "description": "The index (or name) of the desired subsystem. If left blank, will use the first available one.",
                "type": 10,
                "required": false,
            },
            {
                "name": "image",
                "description": "Attach an optional image to the post.",
                "type": 11,
                "required": false,
            }
        ]
    },{
        "name": "latch",
        "description": "Allows you to keep typing as yourself without having to proxy again.",
        "options": [
            {
                "name": "message",
                "description": "What you want to say.",
                "type": 3,
                "required": true,
            },
            {
                "name": "image",
                "description": "Attach an optional image to the post.",
                "type": 11,
                "required": false,
            }
        ]
    },{
        "name": "adduser",
        "description": "Adds a user to your system.",
        "options": [
            {
                "name": "username",
                "description": "The username to add to your system.",
                "type": 3,
                "required": true,
            },{
                "name": "subsystem",
                "description": "The index (or name) of the desired subsystem. If left blank, will use the first available one.",
                "type": 10,
                "required": false,
            },
        ]
    },{
        "name": "deluser",
        "description": "Deletes a user from your system.",
        "options": [
            {
                "name": "username",
                "description": "The username to delete from your system.",
                "type": 3,
                "required": true,
            },{
                "name": "subsystem",
                "description": "The index (or name) of the desired subsystem. If left blank, will use the first available one.",
                "type": 10,
                "required": false,
            },{
                "name": "all",
                "description": "Enable removing all members from the subsystem?",
                "type": 5,
                "required": false,
            }
        ]
    },{
        "name": "Reply (Latch)",
        "type": 3,
    },{
        "name": "Reply (User)",
        "type": 3,
    },{
        "name": "change",
        "description": "Changes things about a user in a subsystem.",
        "options": [
            {
                "name": "user",
                "description": "The user in the subsystem you want to change things for.",
                "type": 3,
                "required": true,
            },
            {
                "name": "subsystem",
                "description": "The subsystem you want to change things for a user in.",
                "type": 10,
                "required": false,
            },
            {
                "name": "name",
                "description": "The user's desired name.",
                "type": 3,
                "required": false,
            },
            {
                "name": "birthday",
                "description": "Birthday in any format you desire.",
                "type": 3,
                "required": false,
            },
            {
                "name": "tag",
                "description": "If provided, changes the tag for the entire subsystem.",
                "type": 3,
                "required": false,
            },
            {
                "name": "color",
                "description": "Color in hex format, without the #.",
                "type": 3,
                "required": false,
            },
            {
                "name": "thumbnail",
                "description": "Image URL to change this user's thumbnail to.",
                "type": 3,
                "required": false,
            },
            {
                "name": "misc",
                "description": "Miscellaneous things about this user.",
                "type": 3,
                "required": false,
            },
            {
                "name": "pronouns",
                "description": "Pronouns for this user.",
                "type": 3,
                "required": false,
            }
        ]
    },{
        "name": "about",
        "description": "Retrieves info on a headspace, subsystem, or individual user, given a tag.",
        "options": [
            {
                "name": "tag",
                "description": "The tag to view info for.",
                "type": 3,
                "required": true,
            },
            {
                "name": "headspace",
                "description": "View headspace info?",
                "type": 5,
                "required": false,
            },
            {
                "name": "subsystem",
                "description": "If provided, gets info for a subsystem.",
                "type": 10,
                "required": false,
            },
            {
                "name": "user",
                "description": "If a username or alias is provided, gets info for that user.",
                "type": 3,
                "required": false,
            }
        ]
    },{
        "name": "importsystem",
        "description": "Imports a system.json from PluralKit.",
        "options": [
            {
                "name": "jsonurl",
                "description": "The system.json file's URL, on a file hosting service like 0x0.st.",
                "type": 3,
                "required": true,
            },
            {
                "name": "subsystem",
                "description": "The index (or name) of the desired subsystem. If left blank, will use the first available one.",
                "type": 10,
                "required": false,
            },
        ]
    },{
        "name": "invite",
        "description": "Allows you to invite the bot to your own server."
    }]
};