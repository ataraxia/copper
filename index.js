const cnf = require('./cnf.json');
const { Slashes } = require('./slashes.js');
const { SlashCompanions } = require('./companions.js');

const { Client, Partials, Routes, GatewayIntentBits, ActivityType, InteractionType } = require("discord.js");
const { REST } = require("@discordjs/rest");

const client = new Client({intents:  [GatewayIntentBits.DirectMessages,
                                      GatewayIntentBits.Guilds,
                                      GatewayIntentBits.GuildMessages,
                                      GatewayIntentBits.MessageContent,
                                      GatewayIntentBits.GuildIntegrations], 
                           partials: [Partials.Channel,
                                      Partials.Message,
                                      Partials.User] });
const rest = new REST({ version: '10' }).setToken(cnf.token);

(async () => {
    try {
        console.log("Refreshing Copper's commands...");
        await rest.put(
            Routes.applicationGuildCommands(cnf.botId, cnf.guildId),
            { body: Slashes.main },
        );
        console.log("Copper's commands are refreshed, now loading the bot...");
    } catch(error) {
        console.error(error);
    }
})();

client.once("ready", () => {
    console.log("Copper is ready as %s\n\nRemember, copying is an act of love.\nPlease copy and share: https://codeberg.org/ataraxia/copper", client.user.tag);
    client.user.setActivity("metalworking",{type: ActivityType.Competing});
});

client.on("interactionCreate", interaction => {
    var interactionName = "";
    if(interaction.type === InteractionType.ModalSubmit) {
        interactionName = interaction.customId;
    } else {
        interactionName = interaction.commandName;
    }
    for(const companion of SlashCompanions) {
        if(companion.name === interactionName) {
            companion.execute(interaction);
        }
    }
})

client.login(cnf.token);